@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Produk</div>

                <div class="panel-body">
                    <a href="\product_add">
                        <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Product</button>
                    </a>
                    <br><br>
                    <table class="table table-borderd" id="DataTable">
                        <thead>
                            <tr>
                                <td width="50px">No</td>
                                <td>Product Code</td>
                                <td>Product Name</td>
                                <td>Product Price</td>
                            </tr>                            
                        </thead>
                        <tbody>
                            @foreach($produk as $p => $list)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $list->product_code }}</td>
                                <td>{{ $list->product_name }}</td>
                                <td>{{ $list->product_price }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#DataTable").DataTable();
    </script>
@endsection
