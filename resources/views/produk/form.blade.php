@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Add Product</div>
    
            <div class="panel-body">
                <div class="col-md-6">
                    <form method="POST" action="\product_save" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3">Product Category</label>
                            <div class="col-md-9">
                                <select name="product_category" id="product_category" class="form-control" onchange="getProductCode()">
                                    <option value="">-- Product Category --</option>
                                    @foreach($category as $c => $cate)
                                        <option value="{{ $cate->category_code }}">{{ $cate->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Product Merk</label>
                            <div class="col-md-9">
                                <select name="product_merk" id="product_merk" class="form-control" onchange="getProductCode()">
                                    <option value="">-- Product Merk --</option>
                                    @foreach($merk as $m => $me)
                                        <option value="{{ $me->merk_code }}">{{ $me->merk_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Product Code</label>
                            <div class="col-md-9">
                                <input type="text" readonly="" name="product_code" id="product_code" value="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Product Name</label>
                            <div class="col-md-9">
                                <input type="text" name="product_name" id="product_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Product Price</label>
                            <div class="col-md-9">
                                <input type="text" name="product_price" id="product_price" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">&nbsp;</label>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                                <a href="\product">
                                    <button type="button" class="btn btn-danger">Back</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function getProductCode(){
        var dataString = {
            _token : $("input[name=_token]").val(),
            category : $("#product_category").val(),
            merk : $("#product_merk").val()
        };
        //var merk = $("#product_merk").val();
        $.ajax({
            url:"{{ url('\product_get_code') }}",
            type: "POST",
            data: dataString,
            dataType : "JSON",
            success:function(data){
                if(data.msg==1){
                    $("#product_code").val(data.product_code);
                }else if(data.msg==0){
                    return false;
                }
            } ,
            error:function(){
                return false;
            }
        });
    }
</script>
@endsection
