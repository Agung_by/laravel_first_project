@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Siswa</div>

                <div class="panel-body">

                    <table class="table table-borderd" id="DataTable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>NIM</td>
                                <td>Nama</td>
                                <td>Kelas</td>
                            </tr>                            
                        </thead>
                        <tbody>
                            {{ $no = 1 }}
                            @foreach($siswa as $s => $ss)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $ss->nim }}</td>
                                <td>{{ $ss->nama }}</td>
                                <td>{{ $ss->kelas }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#DataTable").DataTable();
    </script>
@endsection
