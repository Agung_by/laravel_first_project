<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>laravel Test</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts/fonts.googleapis.com.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
    <script src="{{ asset('js/jquery.2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/ace.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
</head>
<body class="no-skin">
    <div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar">
        <div class="navbar-container" id="navbar-container">
            <div class="navbar-header pull-left">
                <button data-target="#sidebar2" type="button" class="pull-left menu-toggler navbar-toggle">
                    <span class="sr-only">Toggle sidebar</span>

                    <i class="ace-icon fa fa-dashboard white bigger-125"></i>
                </button><a href="{{ url('/home') }}" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    LARAVEL TRAINING
                </small>
            </a>
            <button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
        </div>
    </div>
</div>
<div class="main-container" id="main-container">
    <div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
        <ul class="nav nav-list" style="top: 0px;">
            <li class="hover">
                <a href="{{ url('/home') }}">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="hover">
                <a href="{{ url('/siswa') }}">
                    <i class="menu-icon fa fa-list-ul"></i>
                    <span class="menu-text"> Data Siswa </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="hover">
                <a href="{{ url('/product') }}">
                    <i class="menu-icon fa fa-list-ul"></i>
                    <span class="menu-text"> Data Produk </span>
                </a>

                <b class="arrow"></b>
            </li>

            <li class="hover">
                <a href="Logout">
                    <i class="menu-icon fa fa-sign-out"></i>
                    <span class="menu-text"> Log Out </span>
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </div>
    <div class="main-content" style=" background-repeat: no-repeat; background-size: 100% auto; min-height: 700px">
        <div class="main-content-inner" style="padding: 30px; width: 100%; margin: 0auto">
            <div >
                @yield('content')
            </div>
        </div>
    </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
<div style="background: #e4e6e9; position: absolute; width: 100%; height: 15px;  bottom: 0;z-index: 999999; border-top: #e7e7e7e">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder"></span>
                
            </span>
        </div>
    </div>
</div>
<div style="background: #1777d3; position: absolute; width: 100%; height: 40px;  bottom: 0;z-index: 999999; text-align: center;font-weight: bold;color: white; line-height: 40px">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder"></span>
                by Agung 2017
            </span>
        </div>
    </div>
</div>
</body>
</html>
