<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produk extends Model
{
    protected $table = "product";


	protected $fillable = [
		'product_code', 'product_name', 'product_price',
	];

}
