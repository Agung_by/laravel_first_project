<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produk;
use App\category;
use App\merk;
use App\Helpers\AutoNumber as Helpers;

class ProdukController extends Controller
{
    function index(){
    	$data = produk::all();
    	return view('produk.data')
    	->with('produk',$data)
    	->with('no',1);
    }

    function addProduct(){
        $category = category::all();
        $merk = merk::all();
        return view('produk.form')
        ->with('category',$category)
        ->with('merk',$merk);
    }

    function saveProduct(Request $request){
    	$produk = new produk;
      $produk->product_code = $request->product_code;
      $produk->product_category = $request->product_category;
      $produk->product_merk = $request->product_merk;
      $produk->product_name = $request->product_name;
      $produk->product_price = $request->product_price;
      $produk->save();
      return redirect('product');
  }

  function getProductCode(Request $request){
   $table="product";
   $primary="product_code";
   $category = $request->category;
   $merk = $request->merk;
   if(($category!="")&&($merk!="")){
       $prefix = $category."-".$merk."-";
       $kodeBarang=\App\Helpers\AutoNumber::autoNumber($table,$primary,$prefix);
       $data['msg'] = 1; 
       $data['product_code'] = $kodeBarang; 
       echo json_encode($data);
   }else{
        $data['msg'] = 0;
        echo json_encode($data);
   }
}

}
